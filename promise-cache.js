const promiseCache = (promFn) => {
  let cachedPromise = promFn();
  return (refreshCache = false) => {
    if (refreshCache) cachedPromise = promFn();
    return cachedPromise;
  }
}

// Pass a function that generates a promise instead of the promise, so it can be refreshed
const cachedPromise = promiseCache(() => Promise.resolve(Math.random()));

cachedPromise().then(console.log);                               // 0.3297345352324234
setTimeout(() => cachedPromise().then(console.log), 1000);       // 0.3297345352324234
setTimeout(() => cachedPromise(true).then(console.log), 2000);   // 0.7619858094911067
setTimeout(() => cachedPromise().then(console.log), 3000);       // 0.7619858094911067
